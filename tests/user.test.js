const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
  await dbHandler.connect()
})
afterEach(async () => {
  await dbHandler.clearDatabase()
})
afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete = {
  username: '60160121',
  name: 'นางสาวเจิมขวัญ อยู่ยิ่ง',
  email: '60160121@go.buu.ac.th',
  password: 'cherm123456'
}
const userErrorUsernameEmpty = {
  username: '',
  password: 'cherm123456'
}
const userErrorPasswordEmpty = {
  username: '60160121',
  password: ''
}
const userErrorUsernameEmptyAndPasswordEmpty = {
  username: '',
  password: ''
}
const userErrorUsernameLess4Alphabets = {
  username: '601',
  password: 'cherm123456'
}
const userErrorUsernameMore10Alphabets = {
  username: '6016012160160121',
  password: 'cherm123456'
}
const userErrorPasswordLess4Alphabets = {
  username: '60160121',
  password: 'che'
}
const userErrorPasswordMore20Alphabets = {
  username: '60160121',
  password: 'cherm123456cherm123456'
}
const userErrorUsernameLess4AlphabetsAndPasswordLess4Alphabets = {
  username: '601',
  password: 'che'
}
const userErrorUsernameMore10AlphabetsAndPasswordMore20Alphabets = {
  username: '6016012160160121',
  password: 'cherm123456cherm123456'
}
const userErrorUsernameLess4AlphabetsAndPasswordMore20Alphabets = {
  username: '601',
  password: 'cherm123456cherm123456'
}
const userErrorUsernameMore10AlphabetsAndPasswordLess4lphabets = {
  username: '6016012160160121',
  password: 'che'
}
const userErrorPasswordIncorrect = {
  username: '60160121',
  password: 'abc'
}
const userErrorUsernameIncorrect = {
  username: '60160121@go.buu.com',
  password: 'cherm123456'
}
const userErrorUsernameAndPasswordIncorrect = {
  username: '60160121@go.buu.com',
  password: 'abc'
}
describe('Announce', () => {
  it('สามารถlogin ได้', async () => {
    let error = null
    try {
      const user = new User(userComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username เป็นค่าว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ password เป็นค่าว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorPasswordEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username และ password เป็นค่าว่าง', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameEmptyAndPasswordEmpty)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameLess4Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username มากกว่า 10 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameMore10Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ password น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorPasswordLess4Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ password มากกว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorPasswordMore20Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username น้อยกว่า 4 ตัวอักษร และ password น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameLess4AlphabetsAndPasswordLess4Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username มากกว่า 10 ตัวอักษร และ password มากกว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameMore10AlphabetsAndPasswordMore20Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username น้อยกว่า 4 ตัวอักษร และ password มากกว่า 20 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameLess4AlphabetsAndPasswordMore20Alphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username มากกว่า 10 ตัวอักษร และ password น้อยกว่า 4 ตัวอักษร', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameMore10AlphabetsAndPasswordLess4lphabets)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ password ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new User(userErrorPasswordIncorrect)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameIncorrect)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถ login ได้เพราะ username และ password ไม่ถูกต้อง', async () => {
    let error = null
    try {
      const user = new User(userErrorUsernameAndPasswordIncorrect)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
