const express = require('express')
const router = express.Router()
const usersController = require('../controller/UserController')

router.get('/', usersController.getUsers)
router.get('/:id', usersController.getUser)

module.exports = router
