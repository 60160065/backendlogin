const User = require('../models/User')
const loginController = {
  login (req, res, next) {
    const username = req.body.username
    const password = req.body.password
    console.log('username: ' + username)
    User.find({ username: username, password: password }).then(function (User) {
      if (typeof User[0] === 'undefined') {
        res.status(500).send('username or password invalid')
      } else {
        res.json(User[0])
        console.log(User[0])
      }
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = loginController
